import React from "react";
import 'antd/dist/antd.css';
import './index.css';
import { Input,Button,Typography} from 'antd';
import './List.css';
const {  Paragraph} = Typography;

const ListItem = props => (

  <div className="list">
    {props.items.map((item, index) => (
      <Typography>
      <Paragraph className='P'
        key={index}
      >
             <Input className='input' onChange={(e)=>{
             props.setUpdate(e.target.value,index)}}
             value={item}/>
             
             <Button className='button' type="submit" 
              onClick={props.deleteTask.bind(this, index)} size='small'>
              Delete
            </Button>
      </Paragraph>
      </Typography>
    ))}
  </div>
);

export default ListItem;
