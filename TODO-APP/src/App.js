import React, { Component } from 'react';
import ListItem from './ListItem';
import 'antd/dist/antd.css';
import './index.css';
import './App.css';
import { Input,Button ,Form} from 'antd';
class App extends Component {
  constructor(props) {
    super(props)

    this.state = {
      term: "",
      text: "",
      items: []
    }
    this.setUpdate = this.setUpdate.bind(this);
  }
  onChangeHandler = e => {
    this.setState({
      term: e.target.value
    });
  };

  onFormSubmitHandler = e => {
    e.preventDefault();
    this.setState({
      term: "",
      items: [...this.state.items, this.state.term]
    });
  };

  onDeleteHandler = index => {
    const deleteTask = [...this.state.items];
    deleteTask.splice(index, 1);
    this.setState({
      items: deleteTask
    });
  };
  setUpdate(text, key) {
    console.log("items:" + this.state.items);
    console.log("key:" + key);
    console.log("key:" + text);
    const items = this.state.items;
    items[key] = text;
    this.setState({
      items: items
    })
  }

  render() {
    return (
      <div className="App">
        <Form onFinish={this.onFormSubmitHandler}>
            <Input className='inp' placeholder="ADD TO-DO..." onChange={this.onChangeHandler}
              value={this.state.term} />
            <Button className='button'type="submit" 
              onClick={this.onFormSubmitHandler} size='middle'>
              Submit
            </Button>
          </Form>
        <ListItem deleteTask={this.onDeleteHandler} items={this.state.items} setUpdate={this.setUpdate} />
      </div>
    );
  }
}
export default App;